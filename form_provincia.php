<?php
// Datos constantes.
    include ('config.php');
?>
 
<html>
    <head>
        <title>Provincias Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
     <h1 id='titulo'><?=Config::$titulo?></h1>
    <div>ALTA PROVINCIA</div>
        <form name="form1" method="post" action="alta_provincia.php">
            <table> 
                <tr>
                    <td>Código:</td><td><input type="text" name="codigo"><br></td>
                </tr>
            <tr>
                    <td>Nombre:</td><td><input type="text" name="nominacion"><br></td>
                </tr>
            <tr>
                    <td>Superficie:</td><td><input type="text" name="superficie"><br></td>
                </tr>
            <tr>
                    <td>Habitantes:</td><td><input type="text" name="habitantes"><br></td>
                </tr>
           <tr>
                    <td>Comunidad:</td><td><input type="text" name="comunidad"><br></td>
                </tr>
            <tr>
                    <td><input type="submit" value="Enviar"> </td>         
                    <td><input type="reset" value="Borrar"></td>
                </tr>            
                            
            </table>
        </form> 
    <a id='inicio' href='index.php'>Inicio</a>
    <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>      
    </body>
</html>

