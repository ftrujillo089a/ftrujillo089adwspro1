<?php
    error_reporting(E_ALL ^ E_NOTICE);
 // Datos constantes.
    include 'config.php';
    include_once("Poblacion.php"); //clase Poblacion
    include_once("alta_instalacion.php");       
    
    function mostrar($obj_poblacion){
     ?>   
                <tr>
                    <td><?=$obj_poblacion->getCodigo()?></td>
                    <td><?=$obj_poblacion->getCodigo_provincia()?></td>
                    <td><?=$obj_poblacion->getNominacion()?></td>
                    <td><?=$obj_poblacion->getSuperficie()?></td>
                    <td><?=$obj_poblacion->getHabitantes()?></td>
                    <td><?=$obj_poblacion->getGobierno()?></td>
                 </tr>                                                    
     <?php   
    }
?>
<html>
    <head>
        <title>Poblaciones Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
     <h1 id='titulo'><?=Config::$titulo?></h1>
     <div>GESTIÓN de POBLACIONES</div><br>
    
    <h4>Listado:</h4>
    <table> 
       <tr>
	<th>Código</th>
        <th>Código Provincia</th>
	<th>Nombre</th>
	<th>Superficie</th>
	<th>Habitantes</th>
        <th>Gobierno</th>

    </tr>
    <?php
    if (Config::$modelo=='fichero') {
      $file = fopen("poblaciones.txt", "r");
            
            while (!feof($file)){
           $linea= fgets($file) ;
        //  dividir en variables (separación ;). Cada elemento del array tiene un dato
         
           $array_datos=  explode(';', $linea);
             
           $obj_poblacion=new Poblacion($array_datos[0], $array_datos[1], $array_datos[2],
                   $array_datos[3], $array_datos[4], $array_datos[5]);
       
            
                mostrar($obj_poblacion);                                                  
     
          }
            fclose($file);
    } // fin modelo=fichero
    
      if (Config::$modelo=='mysql') {
        // establecer conexión con la base de datos
          $conexion=conectarMySQL(Config::$bdnombre); // con base de datos
          $consulta='SELECT * FROM POBLACION';
         
          try {
          $datos=$conexion->query($consulta);
          if ($datos) {
              foreach ($datos as $registro) {
                
             $obj_poblacion=new Poblacion($registro[codigo], $registro[codigo_provincia], 
              $registro[nominacion],$registro[superficie], $registro[habitantes], $registro[gobierno]);
           
             mostrar($obj_poblacion);    
      
              }
              }
          
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        } 
        
        $conexion=NULL;  //cerrar
    }
    ?>
      </table>
    <a id='opciones' href="form_poblacion.php">Altas</a>
    <a href='index.php'>Inicio</a>
    <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>     
    </body>
</html>
