<?php
        // put your code here
        include 'config.php'; 
          ?>
<!DOCTYPE html>
<!--
 Mantenimiento de provincias y sus poblaciones 
-->
     
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/proyecto1.css">
        <title>Provincias Españolas</title>
    </head>
    <body>
  
        <h1 id="titulo"><?=Config::$titulo?></h1>
        <ul>
            <li>Opciones:</li>
            <li><a href="gestion_provincia.php">Gestión Provincias</a></li>
            <li><a href="gestion_poblacion.php">Gestión Problaciones</a></li>
            <li><a href="gestion_instalacion.php">Instalación</a></li>
        </ul>
            
        <ul>
            <li>Documentación por tema:</li> 
              <li><a href="documentacion/DWST5PROYECTO5.pdf">Tema5 POO.Enunciado</a></li>
              <li><a href="documentacion/DWST6PROYECTO6.pdf">Tema6 MYSQL.Enunciado</a></li>
        </ul>
            
           <ul>
               <li>Documentación de este proyecto:</li>
               <li><a href="documentacion/proyecto1.pdf">Documentación</a></li>
           </ul>     
            
        </ul>
        <div id="pie"><?=Config::$autor?> <?Config::$fecha)?> <?=Config::$empresa?> <?=Config::$curso?></div>    
    </body>                   
 
       
    </body>
</html>
