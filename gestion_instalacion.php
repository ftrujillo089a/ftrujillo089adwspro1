<?php
    error_reporting(E_ALL ^ E_NOTICE);
 // Datos constantes.
    include_once ('config.php');
    include_once("Instalacion.php"); //clase Instalacion
    include_once("alta_instalacion.php"); //clase  
                
?>
<html>
    <head>
        <title>Poblaciones Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
     <h1 id='titulo'><?=Config::$titulo?></h1>
     <div>Instalando: mySQL</div><br>
    
       <?php
      //  crear objeto instalacion
    $baseDatos =  Config::$bdnombre;
    $tabla ='provincia';
    $tabla2='poblacion';
    $clavePrimaria ='codigo';
    $claveAjena='';
         
    $obj_instalacion= new Instalacion($baseDatos, $tabla, $tabla2, $clavePrimaria, $claveAjena);
     ejecutar($obj_instalacion);
     ?>                                                          
     
      
     <a href='index.php'>Inicio</a>
    <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>     
    </body>   
    </body>
</html>