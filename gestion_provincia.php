<?php
    error_reporting(E_ALL ^ E_NOTICE);
 // Datos constantes.
    include 'config.php';
    include_once("Provincia.php"); 
    include_once("alta_instalacion.php");  
    
    function mostrar($obj_provincia){
     ?>   
                <tr>
                    <td><?=$obj_provincia->getCodigo()?></td>
                    <td><?=$obj_provincia->getNominacion()?></td>
                    <td><?=$obj_provincia->getSuperficie()?></td>
                    <td><?=$obj_provincia->getHabitantes()?></td>
                    <td><?=$obj_provincia->getComunidad()?></td>
                 </tr>                                                    
     <?php
    }
?>
<html>
    <head>
        <title>Provincias Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
     <h1 id='titulo'><?=Config::$titulo?></h1>
     <div>GESTIÓN de PROVINCIAS</div><br>
    
    <h>Listado:</h>
    <table> 
       <tr>
	<th>Código</th>
	<th>Nombre</th>
	<th>Superficie</th>
	<th>Habitantes</th>
        <th>Comunidad</th>

    </tr>
    <?php
    if (Config::$modelo=='fichero') {
      $file = fopen("provincias.txt", "r");
          
            while (!feof($file)){
           $linea= fgets($file) ;
        //  dividir en variables (separación ;). Cada elemento del array tiene un dato
         
           $array_datos=  explode(';', $linea);
        
           $obj_provincia=new Provincia($array_datos[0], $array_datos[1], $array_datos[2],
                   $array_datos[3], $array_datos[4]);
       
          mostrar($obj_provincia);
          }
            fclose($file);
    }// fin modelo=fichero
    
      if (Config::$modelo=='mysql') {
        // establecer conexión con la base de datos
          $conexion=conectarMySQL(Config::$bdnombre); // con base de datos
          $consulta='SELECT * FROM PROVINCIA';
         
          try {
          $datos=$conexion->query($consulta);
          if ($datos) {
              foreach ($datos as $registro) {
                
             $obj_provincia=new Provincia($registro[codigo],$registro[nominacion],
             $registro[superficie], $registro[habitantes], $registro[comunidad]);
           
             mostrar($obj_provincia);    
      
              }
              }
          
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        } 
        
        $conexion=NULL;  //cerrar
    }
    
    ?>
       </table>
    <a  id='inicio' href='index.php'>Inicio</a>
    <a href="form_provincia.php">Altas</a><br>
    <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>     
    </body>
</html>
