<?php

/* 
 clase Instalacion
 */
class Instalacion {
   //Atributos
    private $baseDatos;
    private $tabla;
    private $tabla2;
    private $clavePrimaria;
    private $claveAjena;
        
// constructor    
    public function __construct($baseDatos,$tabla,$tabla2, $clavePrimaria,$claveAjena) {
        $this -> baseDatos = $baseDatos;
        $this -> tabla = $tabla;
        $this -> tabla2 = $tabla2;
        $this -> clavePrimaria = $clavePrimaria;
        $this -> claveAjena = $claveAjena;       
    } 
    
    function getBaseDatos() {
        return $this->baseDatos;
    }

    function getTabla() {
        return $this->tabla;
    }

    function getTabla2() {
        return $this->tabla2;
    }

    function getClavePrimaria() {
        return $this->clavePrimaria;
    }

    function getClaveAjena() {
        return $this->claveAjena;
    }
        function setBaseDatos($baseDatos) {
        $this->baseDatos = $baseDatos;
    }

    function setTabla($tabla) {
        $this->tabla = $tabla;
    }

    function setTabla2($tabla2) {
        $this->tabla2 = $tabla2;
    }

    function setClavePrimaria($clavePrimaria) {
        $this->clavePrimaria = $clavePrimaria;
    }

    function setClaveAjena($claveAjena) {
        $this->claveAjena = $claveAjena;
    }

}
?>